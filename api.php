<?php 
	include("conexion.php");
	$con = con();
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
	<title>BD ESTUDIANTES</title>
</head>
<body>
	<h1 align="center"> ESTUDIANTES </h1>
	<br><table width="70%" border="1px" align="center" border-radius="3px">
		<tr align="center" style="height: 40px;">
			<th>Código estudiante</th>
			<th>Nombres</th>
			<th>Edad</th>
			<th>Telefono</th>
		</tr>
        <?php
			$query = 'SELECT *FROM estudiantes';
            $resultado = pg_query($con, $query) or die("Error en la consulta sql");
            while($fila=pg_fetch_array($resultado)){
		?>
		<tr align="center" style="height: 40px;">
			<td><?php echo $fila['cod_est']?></td>
			<td><?php echo $fila['nomb_est']?></td>
			<td><?php echo $fila['edad']?></td>
			<td><?php echo $fila['tel']?></td>
		</tr>
		<?php
	        }
	    ?>
	</table>
</body>
</html>