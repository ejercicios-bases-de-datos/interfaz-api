CREATE DATABASE bases_api;
CREATE TABLE estudiantes (cod_est SERIAL NOT NULL PRIMARY KEY, nomb_est VARCHAR(50), edad INT, tel VARCHAR(10));
INSERT INTO estudiantes (nomb_est, edad, tel) VALUES ('Nassly Godoy', 19, '3232231974');
INSERT INTO estudiantes (nomb_est, edad, tel) VALUES ('Aleja Colmenares', 19, '3221456789');
INSERT INTO estudiantes (nomb_est, edad, tel) VALUES ('Javier Charry', 21, '3225698974'); 